/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.util;

import com.neuron.exm.rs.Exception.QException;
import java.math.BigInteger;
import java.security.SecureRandom;

/**
 *
 * @author otahmadov
 */
public class IdGenerator {
    private static final SecureRandom RANDOM = new SecureRandom();
    
    public static String nextRandomSessionId() {
        return new BigInteger(130, RANDOM).toString(32);
    }
    
    public static String nextDbId() {
        return new BigInteger(32, RANDOM).toString(32);
    }

    public static String getId(CoreEntity entity) throws QException {
        String res = "";
        res = getIdType1();
        return res;
    }

    public static String getId() throws QException {
            return getIdType1();
    }

    static String capitalizeFirstLetter(String arg) {
        arg = arg.substring(0, 1).toUpperCase() + arg.substring(1, arg.length()).toLowerCase();
        return arg;
    }

    public static String getIdType1() {

        int randomNum = 1000 + (int) (Math.random() * 9999);
        String id = QDate.getCurrentDate().substring(2, 8)  + QDate.getCurrentTime()
                + QDate.getCurrentMillisecond()+String.valueOf(randomNum) ;
        id = id.replace("-", "");
        return id;
    }
    
    
}
