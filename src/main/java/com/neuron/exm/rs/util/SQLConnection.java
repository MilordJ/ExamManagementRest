/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.util;

import com.neuron.exm.rs.Exception.QException;
import com.neuron.exm.rs.domain.Carrier;
import com.neuron.exm.rs.domain.SessionManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;

/**
 *
 * @author otahmadov
 */
public class SQLConnection {

    private static final Logger log = Logger.getLogger(SQLConnection.class);
    
    // Duzeldilmis uygun query e gore insert gedir
    public static void execInsertSql(String arg, ArrayList valueList) throws QException, SQLException {
        Connection conn = SessionManager.getCurrentConnection();
        try {
//            conn.setAutoCommit(false);
            try (PreparedStatement stmt = conn.prepareStatement(arg)) {
                String line = "";
                for (int i = 0; i < valueList.size(); i++) {
                    if(valueList.get(i).toString().trim().length() > 0)
                            stmt.setObject(i + 1, valueList.get(i));
                        else
                            stmt.setNull(i + 1, Types.NULL);
                    line = line + i + "->" + valueList.get(i) + ";";
                }
                log.info(arg + " --> " + line);
                stmt.executeUpdate();
//                conn.commit();
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
//            conn.rollback();
        }
    }
//    public static void execInsertSql(String arg, String[] values) throws QException {
//        try {
//            
//            try (Connection conn = SessionManager.getCurrentConnection();
//                  PreparedStatement stmt = conn.prepareStatement(arg)) {
//                String line = "";
//                for (int i = 0; i < values.length; i++) {
//                    stmt.setObject(i + 1, values[i].trim(), java.sql.Types.OTHER);
//                    line = line + i + "->" + values[i].trim() + ";";
//                }
//                log.info(arg + " --> " + line);
//                stmt.executeUpdate();
//            }
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//        }
//    }

    private static void addValueToStatement(PreparedStatement stmt, int ind, String value, ArrayList array) {
        try {
            stmt.setObject(ind, value);
            array.add(value);
        } catch (SQLException ex) {
            new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(), ex);
        }
    }

    public static Carrier execSelectSql(String arg, String tableName, ArrayList values) throws QException {
        Carrier qc1 = new Carrier();
        ArrayList logLine = new ArrayList();
        try {
            Connection conn = SessionManager.getCurrentConnection();
            try(PreparedStatement stmt = conn.prepareStatement(arg)){

                int idx = 1;
                String line = "";
                String query = arg;
                for (int i = 0; i < values.size(); i++) {
                    if (!values.get(i).toString().trim().equals("")) {
                        String val = values.get(i).toString().trim();
                        
                        stmt.setObject(idx, val);
//                        stmt.setObject(idx, val, java.sql.Types.OTHER);
                        
                        line = line + i + "->" + values.get(i) + ";";
//                        array.add(value);
//                        addValueToStatement(stmt, idx++, val, logLine);
                        ++idx;
                    }
                }
                log.info(query + " --> " + line);
                try (ResultSet rs = stmt.executeQuery()) {
                    qc1 = convertResultSetToCarrier(rs, tableName);
                }
            }
        } catch (SQLException | QException e) {
            log.error(e.getMessage(), e);
        }
        return qc1;
    }

    public static String execSequence(String query) throws QException {
        try {
            Connection conn = SessionManager.getCurrentConnection();
            try(PreparedStatement stmt = conn.prepareStatement(query)){
                try(ResultSet resultSet = stmt.executeQuery()) {
                    if(resultSet.next()) {
                        return resultSet.getString(1);
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }
    public static Carrier execSelectSqlSpec(String arg, String tableName, ArrayList values) throws QException {
        Carrier carrier = new Carrier();
        ArrayList logLine = new ArrayList();
        try {
            Connection conn = SessionManager.getCurrentConnection();
            try(PreparedStatement stmt = conn.prepareStatement(arg)){

                int idx = 1;
                String line = "";
                String query = arg;
                for (int i = 0; i < values.size(); i++) {
                    if (!values.get(i).toString().trim().equals("")) {
                        String val = values.get(i).toString().trim();
                        
                        stmt.setObject(idx, val);
//                        stmt.setObject(idx, val, java.sql.Types.OTHER);
                        
                        line = line + i + "->" + values.get(i) + ";";
//                        array.add(value);
//                        addValueToStatement(stmt, idx++, val, logLine);
                        ++idx;
                    }
                }
                log.info(query + " --> " + line);
                try (ResultSet rs = stmt.executeQuery()) {
                  carrier = convertResultSetToCarrierSpec(rs, tableName);
                }
            }
        } catch (SQLException | QException e) {
            log.error(e.getMessage(), e);
        }
        
        return carrier;
    }
    
    public static String specSelectSql(String arg) throws QException {
        String result="";
        try {
            Connection conn = SessionManager.getCurrentConnection();
            try(PreparedStatement stmt = conn.prepareStatement(arg)){

                try (ResultSet rs = stmt.executeQuery()) {
                    if(rs.next()) return rs.getString(1);
                }
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    public static Carrier execSelectSql(String arg, String tableName, ArrayList values, Connection conn1) throws QException {
        Carrier qc1 = new Carrier();
        ArrayList logLine = new ArrayList();
        try {
            Connection conn = SessionManager.getCurrentConnection();
            
            try(PreparedStatement stmt = conn.prepareStatement(arg)){
                int idx = 1;
                String line = "";
                String query = arg;
                for (int i = 0; i < values.size(); i++) {
                    if (!values.get(i).toString().trim().equals("")) {
                        String val = values.get(i).toString().trim();
    //                    query = query.replace("?", "'"+val+"'");
                        addValueToStatement(stmt, idx++, val, logLine);
                    }
                }
                log.info(query + " --> " + line);
                try (ResultSet rs = stmt.executeQuery()) {
                    qc1 = convertResultSetToCarrier(rs, tableName);
                }
                stmt.close();
            }
        } catch (SQLException | QException e) {
           log.error(e.getMessage(), e);
        }
        return qc1;
    }

    public static void execUpdateSql(String arg, ArrayList valueList) throws QException, SQLException {
        Connection conn = SessionManager.getCurrentConnection();
        try {
            
//            conn.setAutoCommit(false);
            try(PreparedStatement stmt = conn.prepareStatement(arg)){
                String line = "";
                for (int i = 0; i < valueList.size(); i++) {
                    String value = valueList.get(i).toString().trim();
                        if(value.length() > 0)
                            stmt.setObject(i + 1, value);
//                            stmt.setObject(i + 1, value, java.sql.Types.OTHER);
                        else
                            stmt.setNull(i + 1, Types.NULL);
                        line = line + i + "->" + value + ";";
                }
                log.info(arg + " --> " + line);
                stmt.executeUpdate();
//                conn.commit();
            }    
        } catch (Exception e) {
            log.error(e.getMessage(), e);
//            conn.rollback();
        }
    }
    public static void execDeleteSqlWithoutActive0(String arg, ArrayList valueList) throws QException {
        try {

            Connection conn = SessionManager.getCurrentConnection();
            try(PreparedStatement stmt = conn.prepareStatement(arg)){
                String line = "";
                for (int i = 0; i < valueList.size(); i++) {
                    String value = valueList.get(i).toString().trim();
                        stmt.setObject(i + 1, value);
//                        stmt.setObject(i + 1, value, java.sql.Types.OTHER);
                        line = line + i + "->" + value + ";";
                }
                log.info(arg + " --> " + line);
                stmt.executeUpdate();
            }    
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public static Carrier execDeleteSql(String query, String[] values) throws QException {
        try {
            Connection conn = SessionManager.getCurrentConnection();
            try(PreparedStatement preparedStatement = conn.prepareStatement(query)){
                for (int i = 0; i < values.length; i++) {
                    preparedStatement.setObject(i + 1, values[i].trim());
//                    preparedStatement.setObject(i + 1, values[i].trim(), java.sql.Types.OTHER);
                }
                
                preparedStatement.executeUpdate();
            }    
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
//        System.out.println("Ok.");
        return null;
    }

    private static String[] getColumnNames(ResultSet rs) throws QException {
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            String[] columnNames = new String[columnCount];
            // The column count starts from 1
            for (int i = 1; i < columnCount + 1; i++) {
                columnNames[i - 1] = rsmd.getColumnName(i);
            }
            return columnNames;
        } catch (Exception e) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(),
                    e);

        }

    }

    private static String getTableName(ResultSet rs) throws QException {
        String cc = "";
        try {
            rs.next();
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnCount = rsmd.getColumnCount();
            String[] columnNames = new String[columnCount];
            cc = rsmd.getTableName(1) + rsmd.getTableName(2);
        } catch (Exception e) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(),
                    e);
        }
 
        return "table";
    }

    private static Carrier convertResultSetToCarrier(ResultSet rs, String tableName) throws QException {
        String[] colNames = getColumnNames(rs);
        Carrier qCarry1 = new Carrier();
        String currentLang = SessionManager.getCurrentLang();
        try {   
            int row = 0;
            while (rs.next()) {
                for (int i = 0; i <= colNames.length - 1; i++) {
                    
//                    String[] currentLangName = colNames[i].split("_");
//                    if(currentLangName.length > 1) {
//                        if(currentLangName[currentLangName.length - 1].toLowerCase().equals(currentLang)) {
//                            String vl = rs.getString(colNames[i]) == null ? "" : rs.getString(colNames[i]).trim();
//                            String fieldName = convertTableFieldNameToEntityfieldName(colNames[i].substring(0,colNames[i].trim().length() - 3));
//
//                            qCarry1.setValue(tableName, row, fieldName, vl);  
//                            
//                            
//                        }
//                    }
                    String vl = rs.getString(colNames[i]) == null ? "" : rs.getString(colNames[i]).trim();
                    String fieldName = convertTableFieldNameToEntityfieldName(colNames[i]);
                            
                    qCarry1.setValue(tableName, row, fieldName, vl);
//                    qCarry1.setValue(CoreLabel.RESULT_SET, row, fieldName, vl);
                }
                row++;
            }
        } catch (SQLException | QException e) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(),
                    e);
        }
        return qCarry1;
    }
    private static Carrier convertResultSetToCarrierSpec(ResultSet rs, String tableName) throws QException { // Hansisa melumat icerisinde list ile qaytaririrqsa o zaman istifade edilir
        String[] colNames = getColumnNames(rs);
//        Carrier qCarry1 = SessionManager.sessionCarrier();
        Carrier qCarry1 = new Carrier();
        String currentLang = SessionManager.getCurrentLang();
        try {   
            int row = 0;
            while (rs.next()) {
                for (int i = 0; i <= colNames.length - 1; i++) {
                    
//                    String[] currentLangName = colNames[i].split("_");
//                    if(currentLangName.length > 1) {
//                        if(currentLangName[currentLangName.length - 1].toLowerCase().equals(currentLang)) {
//                            String vl = rs.getString(colNames[i]) == null ? "" : rs.getString(colNames[i]).trim();
//                            String fieldName = convertTableFieldNameToEntityfieldName(colNames[i].substring(0,colNames[i].trim().length() - 3));
//
//                            qCarry1.setValue(tableName, row, fieldName, vl);  
//                            
//                            
//                        }
//                    }
                    String vl = rs.getString(colNames[i]) == null ? "" : rs.getString(colNames[i]).trim();
                    String fieldName = convertTableFieldNameToEntityfieldName(colNames[i]);
                            
                    qCarry1.setValue(tableName, row, fieldName, vl);
//                    qCarry1.setValue(CoreLabel.RESULT_SET, row, fieldName, vl);
                }
                row++;
            }
        } catch (SQLException | QException e) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(),
                    e);
        }
        return qCarry1;
    }

    private static Carrier convertResultSetToCarrierBoth(ResultSet rs, String tableName) throws QException {
        String[] colNames = getColumnNames(rs);
        //String 	tableName = getTableName(rs);
        Carrier qCarry1 = new Carrier();
        try {
            int row = 0;
            while (rs.next()) {
                for (int i = 0; i <= colNames.length - 1; i++) {
                    qCarry1.setValue(tableName, row, i, rs.getString(colNames[i]).trim());
                    qCarry1.setValue(tableName, row, convertTableFieldNameToEntityfieldName(colNames[i]), rs.getString(colNames[i]).trim());
                }
                row++;
            }
        } catch (SQLException | QException e) {
            throw new QException(new Object() {
            }.getClass().getEnclosingClass().getName(),
                    new Object() {
            }.getClass().getEnclosingMethod().getName(),
                    e);
        }
        return qCarry1;
    }

    static String convertTableFieldNameToEntityfieldName(String arg) {
        String UNDERSCORE = "_";
        String st[] = arg.split(UNDERSCORE);
        String res = st[0].toLowerCase();
        for (int i = 1; i <= st.length - 1; i++) {
            res = res + st[i].substring(0, 1).toUpperCase() + st[i].substring(1, st[i].length()).toLowerCase();
        }
        return res;
    }
}
