/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.util;

/**
 *
 * @author otahmadov
 */
public class OrganizationStructureCode {
    public static String TN = "1000072";
    public static String TN_FORMULA = "1000072";
    public static String SCHOOL = "1000056";
    public static String SCHOOL_FORMULA = "1000072*1000056";
    public static String COLLEGE = "1000055";
    public static String COLLEGE_FORMULA = "1000072*1000055";
    public static String SECTOR = "5000000";
    public static String SECTOR_FORMULA = "1000072*5000000";
    public static String ATM = "1000073";
    public static String ATM_FORMULA = "1000072*1000073";
    public static String BRANCH = "1000418";
    public static String BRANCH_FORMULA = "1000072*1000073*1000418";
    public static String FACULTY = "1000083";
    public static String FACULTY_FORMULA = "1000072*1000073*1000418*1000083";
    public static String DEPARTMENT = "1000074";
    public static String DEPARTMENT_FORMULA = "1000072*1000073*1000418*1000083*1000074";
    public static String MASTER_SPEC = "1000604";
    public static String MASTER_SPEC_FORMULA = "1000072*1000073*1000418*1000083*1000604";
    public static String BACHOLAVR_SPEC = "1000057";
    public static String BACHOLAVR_SPEC_FORMULA = "1000072*1000073*1000418*1000083*1000057";
    public static String MASTER_SPEC_S = "1002306";
    public static String MASTER_SPEC_S_FORMULA = "1000072*1000073*1000418*1000083*1000604*1002306";
    public static String BACHOLAVR_SPEC_S = "1108405";
    public static String BACHOLAVR_SPEC_S_FORMULA = "1000072*1000073*1000418*1000083*1000057*1108405";
    
}
