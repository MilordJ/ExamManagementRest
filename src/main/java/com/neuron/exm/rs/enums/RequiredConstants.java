/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.enums;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 *
 * @author otahmadov
 */
@Component
@PropertySource("classpath:required.properties")
@ConfigurationProperties
@Data
public class RequiredConstants {
    private String insertExam;
    private String updateExam;
    private String insertQuestion;
    private String updateQuestion;
    private String updateQuestionChoises;
    private String deleteQuestion;
    private String insertTicket;
    private String insertTicketQuestion;
    
}
