/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.enums;

/**
 *
 * @author Nazrin
 */
public enum ResultCode {
    OK,
    ERROR,
    UNAUTHORIZED,
    INVALID_PARAMS,
    DUPLICATE_DATA
}
