/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.model;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.EntityExamTicketQuestions;
import com.neuron.cm.domain.EntityExamTicketStructures;
import com.neuron.cm.domain.EntityExamTickets;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityExamV_ExamParticipant_v4;
import com.neuron.cm.view.EntityExamV_Questions_v4;
import com.neuron.cm.view.EntityExamV_TicketQuestions_v4;
import com.neuron.cm.view.EntityExamV_Tickets_v4;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author otahmadov
 */
public class TicketsModel {
    public void insertNewTicket() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String examTypeId = c.getValue("examTypeId").toString();
        String subjectId = c.getValue("subjectId").toString();
        String tableName = "structures";
        int rowCount = c.getTableRowCount(tableName);
        int ticketCount = Integer.parseInt(c.getValue("ticketCount").toString());
        int count = 0;
        Map<Integer, Integer> map = new HashMap<>();
        Map<Integer, Integer> map2 = new HashMap<>();
        List<Integer> list = new ArrayList();
        
        for(int i = 0; i < ticketCount; i++) {
            EntityExamTickets ticket = new EntityExamTickets();
            Carrier c1 = new Carrier();
            c1.setValue("examTypeId", examTypeId);
            c1.setValue("subjectId", subjectId);
            EntityManager.doInsertSpec(ticket, c1);
            String ticketId = ticket.getId();
            if(ticketId == null || ticketId.trim().isEmpty()) {
                SessionManager.sessionCarrier().addError("error", "Xeta bas verdi!!!");
                return;
            }
            
            for(int j = 0; j < rowCount; j++) {
                EntityExamTicketStructures structures = new EntityExamTicketStructures();
                Carrier c2 = new Carrier();
                c2.setValue("ticketId", ticketId);
                c2.setValue("difficultyId", c.getValue(tableName, j, "questLevelId"));
                c2.setValue("typeId", c.getValue(tableName, j, "questTypeId"));
                c2.setValue("specId", c.getValue(tableName, j, "questSpecId"));
                c2.setValue("structuresCount", c.getValue(tableName, j, "questCount"));
                EntityManager.doInsertSpec(structures, c2);
            }
            
           for(int j = 0; j < rowCount; j++)  {
                EntityExamV_Questions_v4 ent = new EntityExamV_Questions_v4();
                String questTopicId = c.getValue(tableName, j, "questTopicId").toString();
                String questSpecId = c.getValue(tableName, j, "questSpecId").toString();
                String questTypeId = c.getValue(tableName, j, "questTypeId").toString();
                String questLevelId = c.getValue(tableName, j, "questLevelId").toString();
                String eduLangId = c.getValue(tableName, j, "eduLangId").toString();
                int questCount = Integer.valueOf(c.getValue(tableName, j, "questCount").toString());
                Carrier carrier = new Carrier();
//                carrier.setValue("questTopicId", questTopicId);
                carrier.setValue("questSpecId", questSpecId);
                carrier.setValue("questTypeId", questTypeId);
                carrier.setValue("questLevelId", questLevelId);
                carrier.setValue("eduLangId", eduLangId);
                ent.setAndStatementFildForIn("QUEST_TOPIC_ID", questTopicId);
                carrier = EntityManager.getEntValuesByCarrier(ent, carrier);
                int questionRowCount = carrier.getTableRowCount(ent.toTableName());
//                map.put(j, questionRowCount);
                if(!map2.containsKey(j)) 
                    map2.put(j, 0);
                int questionCount = map2.get(j);
                int restCount = questionRowCount - questionCount - questCount;// bu suallarin sayinin catib catmamasinin yoxlanilmasi ucundur eger menfi edirse elaveni yeniden gotureceyik
                int forCount = 0;
                for(int k = questionCount; k < questionRowCount; k++) {
                    if(forCount >= questCount) {
                        break;
                    }
                    EntityExamTicketQuestions questions = new EntityExamTicketQuestions();
                    Carrier carrier1 = new Carrier();
                    carrier1.setValue("ticketId", ticketId);
                    carrier1.setValue("questionsId", carrier.getValue(ent.toTableName(), k, "id"));
                    EntityManager.doInsertSpec(questions, carrier1);
                    map2.put(j, map2.get(j) + 1);
                    list.add(k);
                    ++forCount;
                }
                
                if(restCount < 0) {
                    for(int z = 0; z < Math.abs(restCount); z++) {
                        Random random = new Random();
                        int randomNumber = 0;
                        while(true) {
                           randomNumber = random.nextInt(questionRowCount); 
                           if(!list.contains(randomNumber)) {
                               break;
                           }
                        }
                        list.add(randomNumber);
                        EntityExamTicketQuestions questions = new EntityExamTicketQuestions();
                        Carrier carrier1 = new Carrier();
                        carrier1.setValue("ticketId", ticketId);
                        carrier1.setValue("questionsId", carrier.getValue(ent.toTableName(), randomNumber, "id"));
                        EntityManager.doInsertSpec(questions, carrier1);
                    }
                }
                list.clear();
           }
            
        }
        
    }
    
    public void getTicketsList() throws Exception {
        EntityExamV_Tickets_v4 ent = new EntityExamV_Tickets_v4();
        EntityManager.doSelect(ent);
    }
    
    public void deleteTicket() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        Carrier carrier = new Carrier();
        String ticketId = c.getValue("id").toString();
        EntityExamTickets ent = new EntityExamTickets();
        EntityExamTicketStructures structures = new EntityExamTicketStructures();
        EntityExamTicketQuestions questions = new EntityExamTicketQuestions();
        EntityManager.doDelete(ent);
        carrier.setValue("ticketId", ticketId);
        EntityManager.doDeleteByOtherParams(structures, carrier, Arrays.asList("ticketId"));
        carrier = new Carrier();
        carrier.setValue("ticketId", ticketId);
        EntityManager.doDeleteByOtherParams(questions, carrier, Arrays.asList("ticketId"));
    }
    
    public void getTicketQuestions() throws Exception {
        EntityExamV_TicketQuestions_v4 ent = new EntityExamV_TicketQuestions_v4();
        EntityManager.doSelect(ent);
        
    }

}
