/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.model;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.EntityExamQuestionChoises;
import com.neuron.cm.domain.EntityExamQuestions;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityExamV_Questions_v4;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.view.EntityExamV_QuestionChoises_v4;
import java.util.Arrays;

/**
 *
 * @author otahmadov
 */
public class QuestionModel {
    public void insertNewQuestion() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        EntityExamQuestions ent = new EntityExamQuestions();
        EntityManager.doInsert(ent);
        String questId = ent.getId();
        if(questId == null || questId.trim().isEmpty()){
            c.addError("ERROR", "Invalid insertion of quest");
            return;
        }
        String tableName = "choises";
        int rowCount = c.getTableRowCount(tableName);
        for (int i = 0; i < rowCount; i++) {
            EntityExamQuestionChoises choises = new EntityExamQuestionChoises();
            Carrier c1 = new Carrier();
                c1.setValue("questId", questId);
                c1.setValue("rightChoise", c.getValue(tableName, i, "rightChoise"));
                c1.setValue("questChFileId", c.getValue(tableName, i, "questChFileId"));
                c1.setValue("questChContent", c.getValue(tableName, i, "questChContent"));
            EntityManager.doInsertSpec(choises, c1);
        }
    }
    public void updateQuestion() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        EntityExamQuestions ent = new EntityExamQuestions();
        String id = c.getValue("id").toString();
        EntityManager.doUpdate(ent);
        EntityExamQuestionChoises ent1 = new EntityExamQuestionChoises();
        Carrier c1 = new Carrier();
        c1.setValue("questId", id);
        EntityManager.doDeleteByOtherParams(ent1, c1, Arrays.asList("questId"));
        
        String tableName = "choises";
        int rowCount = c.getTableRowCount(tableName);
        for (int i = 0; i < rowCount; i++) {
            
            ent1 = new EntityExamQuestionChoises();
            c1 = new Carrier();
            c1.setValue("questId", id);
            c1.setValue("rightChoise", c.getValue(tableName, i, "rightChoise"));
            c1.setValue("questChFileId", c.getValue(tableName, i, "questChFileId"));
            c1.setValue("questChContent", c.getValue(tableName, i, "questChContent"));
            EntityManager.doInsertSpec(ent1, c1);
        }
    }
    
    public void deleteQuestion() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String questId = c.getValue("id").toString();
        EntityExamQuestions ent = new EntityExamQuestions();
        EntityManager.doDelete(ent);
        Carrier c1 = new Carrier();
        EntityExamQuestionChoises ent1 = new EntityExamQuestionChoises();
        c1.setValue("questId", questId);
        EntityManager.doDeleteByOtherParams(ent1, c1, Arrays.asList("questId"));
    }
    
    public void getQuestionList() throws Exception {
        EntityExamV_Questions_v4 ent = new EntityExamV_Questions_v4();
        EntityManager.doSelect(ent);
    }
        
    public void getQuestionChoisesList() throws Exception {
        EntityExamV_QuestionChoises_v4 ent = new EntityExamV_QuestionChoises_v4();
        EntityManager.doSelect(ent);
    }
    public void getQuestionListForCommon() throws Exception {
        EntityExamV_Questions_v4 ent = new EntityExamV_Questions_v4();
        EntityManager.doSelect(ent);
    }
}
