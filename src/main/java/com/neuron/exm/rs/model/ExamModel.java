/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.model;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.EntityExamExamDetails;
import com.neuron.cm.domain.EntityExamExamParticipant;
import com.neuron.cm.domain.EntityExamExamResult;
import com.neuron.cm.domain.EntityExamRepeatExam;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityExamV_ExamDetails_v4;
import com.neuron.cm.view.EntityExamV_ExamParticipant_v4;
import com.neuron.cm.view.EntityExamV_TicketQuestions_v4;
import com.neuron.cm.view.EntityExamV_Tickets_v4;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Lito
 */
public class ExamModel {

    public void getExamList() throws Exception {
        EntityExamV_ExamDetails_v4 ent = new EntityExamV_ExamDetails_v4();
        EntityManager.doSelect(ent);
    }    
    
    public void getExamParticipantList() throws Exception {
        EntityExamV_ExamParticipant_v4 ent = new EntityExamV_ExamParticipant_v4();
        EntityManager.doSelect(ent);
    }

    public void insertNewExam() throws Exception {
        List list = new ArrayList();
        List list2 = new ArrayList();
        Carrier carrier = SessionManager.sessionCarrier();
        EntityExamExamDetails ent = new EntityExamExamDetails();
        EntityManager.doInsert(ent);
        EntityExamV_Tickets_v4 tickets_v4 = new EntityExamV_Tickets_v4();
        Carrier c = new Carrier();
        String courseId = carrier.getValue("courseId").toString();
        String repetitionId = carrier.getValue("repetition").toString();
        c.setValue("courseId", courseId);
        c.setValue("examTypeId", carrier.getValue("examTypeId"));
        c = EntityManager.getEntValuesByCarrier(tickets_v4, c);
        int ticketCount = c.getTableRowCount(tickets_v4.toTableName());
        final int consTicketCount = ticketCount;
        String examId = ent.getId();
//        String examId = carrier.getValue("id").toString();
        String participants = "participants";
        int partCount = carrier.getTableRowCount(participants);
        Random random = new Random();
        for (int i = 0; i < partCount; i++) {
            EntityExamExamParticipant participant = new EntityExamExamParticipant();
            Carrier c1 = new Carrier();
            String participantId = carrier.getValue(participants, i, "participantId").toString();
            String pelcId = carrier.getValue(participants, i, "pelcId").toString();
            c1.setValue("examDetailId", examId);
            c1.setValue("participantId", participantId);
            while (true) {
                int randomNum = random.nextInt(consTicketCount) + 0;
                String ticketId = c.getValue(tickets_v4.toTableName(), randomNum, "id").toString();
                if (ticketCount > 0) {
                    if (!list.contains(randomNum)) {
                        c1.setValue("ticketId", ticketId);
                        list.add(randomNum);
                        ticketCount--;
                    } else {
                        continue;
                    }
                } else {
                    if (!list2.contains(randomNum)) {
                        c1.setValue("ticketId", ticketId);
                        list2.add(randomNum);
                    } else {
                        continue;
                    }
                }
                EntityManager.doInsertSpec(participant, c1);
                String examParticipantId = c1.getValue("id").toString();
                EntityExamV_TicketQuestions_v4 ticketQuestions = new EntityExamV_TicketQuestions_v4();
                Carrier c3 = new Carrier();
                c3.setValue("ticketId", ticketId);
                c3 = EntityManager.getEntValuesByCarrier(ticketQuestions, c3);
                int qustionCount = c3.getTableRowCount(ticketQuestions.toTableName());
                for (int u = 0; u < qustionCount; u++) {
                    EntityExamExamResult examResult = new EntityExamExamResult();
                    Carrier c4 = new Carrier();
                    String questionId = c3.getValue(ticketQuestions.toTableName(), u, "questionId").toString();
                    c4.setValue("examParticipantId", examParticipantId);
                    c4.setValue("questId", questionId);
                    EntityManager.doInsertSpec(examResult, c4);
                    
                }
                
                if(repetitionId.equals("1")){ //Insertion of repeat exam.
                    EntityExamRepeatExam repeatExam = new EntityExamRepeatExam();
                    Carrier c5 = new Carrier();
                    c5.setValue("examDetailsId", examId);
                    c5.setValue("courseId", courseId);
                    c5.setValue("pelcId", pelcId);
                    c5.setValue("courseEvaTypeId", 1108367);
                    EntityManager.doInsertSpec(repeatExam, c5);
                }
                break;
            }
        }
    }

    public void updateExam() throws Exception {
        ///-------------------- UPDATING PART of EXAM DETAILS ------------------------------//
        Carrier carrier = SessionManager.sessionCarrier();
        String examId = carrier.getValue("id").toString();
        String repetitionId = carrier.getValue("repetition").toString();
        String courseId = carrier.getValue("courseId").toString();
        EntityExamExamDetails ent = new EntityExamExamDetails();

        EntityExamV_ExamParticipant_v4 participant_v4 = new EntityExamV_ExamParticipant_v4();
        Carrier carrier1 = new Carrier();
        carrier1.setValue("examDetailId", examId);
        carrier1 = EntityManager.getEntValuesByCarrier(participant_v4, carrier1);
        int rowCount = carrier1.getTableRowCount(participant_v4.toTableName());

        for (int i = 0; i < rowCount; i++) {
            //--------------------Removing Participants--------------------------//
            EntityExamExamParticipant participant = new EntityExamExamParticipant();
            Carrier carrier2 = new Carrier();
            carrier2.setValue("id", carrier1.getValue(participant_v4.toTableName(), i, "id"));
            EntityManager.doDeleteSpec(participant, carrier2);

            //--------------------Removing from Result--------------------------//   
            EntityExamExamResult examResult = new EntityExamExamResult();
            Carrier carrier3 = new Carrier();
            carrier3.setValue("examParticipantId", carrier1.getValue(participant_v4.toTableName(), i, "id"));
            EntityManager.doDeleteByOtherParams(examResult, carrier3, Arrays.asList("examParticipantId"));
            
        }
        
        //-------------------Removing from Repetition if it is repeat--------//
        if(repetitionId.equals("1")){
            EntityExamRepeatExam repeatExam = new EntityExamRepeatExam();
            Carrier carrier4 = new Carrier();
            carrier4.setValue("examDetailsId", examId);
            EntityManager.doDeleteByOtherParams(repeatExam, carrier4, Arrays.asList("examDetailsId"));
        }
        
        List list = new ArrayList();
        List list2 = new ArrayList();
        EntityExamV_Tickets_v4 tickets_v4 = new EntityExamV_Tickets_v4();
        Carrier c1 = new Carrier();
        c1.setValue("courseId", courseId);
        c1.setValue("examTypeId", carrier.getValue("examTypeId"));
        c1 = EntityManager.getEntValuesByCarrier(tickets_v4, c1);
        int ticketCount = c1.getTableRowCount(tickets_v4.toTableName());
        final int consTicketCount = ticketCount;
        String participants = "participants";
        int partCount = carrier.getTableRowCount(participants);
        Random random = new Random();
        for (int i = 0; i < partCount; i++) {
            EntityExamExamParticipant participant = new EntityExamExamParticipant();
            Carrier c2 = new Carrier();
            String participantId = carrier.getValue(participants, i, "participantId").toString();
            c2.setValue("examDetailId", examId);
            c2.setValue("participantId", participantId);
            while (true) {
                int randomNum = random.nextInt(consTicketCount) + 0;
                String ticketId = c1.getValue(tickets_v4.toTableName(), randomNum, "id").toString();
                if (ticketCount > 0) {
                    if (!list.contains(randomNum)) {
                        c2.setValue("ticketId", ticketId);
                        list.add(randomNum);
                        ticketCount--;
                    } else {
                        continue;
                    }
                } else {
                    if (!list2.contains(randomNum)) {
                        c2.setValue("ticketId", ticketId);
                        list2.add(randomNum);
                    } else {
                        continue;
                    }
                }
                EntityManager.doInsertSpec(participant, c2);
                String examParticipantId = c2.getValue("id").toString();
                String pelcId = c2.getValue("pelcId").toString();
                EntityExamV_TicketQuestions_v4 ticketQuestions = new EntityExamV_TicketQuestions_v4();
                Carrier c3 = new Carrier();
                c3.setValue("ticketId", ticketId);
                c3 = EntityManager.getEntValuesByCarrier(ticketQuestions, c3);
                int qustionCount = c3.getTableRowCount(ticketQuestions.toTableName());
                for (int u = 0; u < qustionCount; u++) {
                    EntityExamExamResult examResult = new EntityExamExamResult();
                    Carrier c4 = new Carrier();
                    String questionId = c3.getValue(ticketQuestions.toTableName(), u, "questionId").toString();
                    c4.setValue("examParticipantId", examParticipantId);
                    c4.setValue("questId", questionId);
                    EntityManager.doInsertSpec(examResult, c4);
                }
                if(repetitionId.equals("1")){ //Insertion of repeat exam.
                    EntityExamRepeatExam repeatExam = new EntityExamRepeatExam();
                    Carrier c5 = new Carrier();
                    c5.setValue("examDetailsId", examId);
                    c5.setValue("courseId", courseId);
                    c5.setValue("pelcId", pelcId);
                    c5.setValue("courseEvaTypeId", 1108367);
                    EntityManager.doInsertSpec(repeatExam, c5);
                }
                break;
            }
        }
        
        EntityManager.doUpdate(ent);
    }

    public void deleteExam() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String examId = c.getValue("id").toString();
        String repetitionId = c.getValue("repetitionId").toString();
        EntityExamExamDetails ent = new EntityExamExamDetails();
        Carrier carrier = new Carrier();
        carrier.setValue("examDetailId", examId);
        EntityExamV_ExamParticipant_v4 participants = new EntityExamV_ExamParticipant_v4();
        carrier = EntityManager.getEntValuesByCarrier(participants, carrier);

        EntityManager.doDelete(ent);

        int rowCount = carrier.getTableRowCount(participants.toTableName());
        for (int i = 0; i < rowCount; i++) {
            Carrier carrier1 = new Carrier();
            EntityExamExamParticipant participant = new EntityExamExamParticipant();
            carrier1.setValue("id", carrier.getValue(participants.toTableName(), i, "id"));
            EntityManager.doDeleteSpec(participant, carrier1);

            Carrier carrier2 = new Carrier();
            carrier2.setValue("examParticipantId", carrier.getValue(participants.toTableName(), i, "id"));
            EntityExamExamResult examResult = new EntityExamExamResult();
            EntityManager.doDeleteByOtherParams(examResult, carrier2, Arrays.asList("examParticipantId"));
        }
        
        if(repetitionId.equals("1")){
            EntityExamRepeatExam repeatExam = new EntityExamRepeatExam();
            Carrier carrier4 = new Carrier();
            carrier4.setValue("examDetailsId", examId);
            EntityManager.doDeleteByOtherParams(repeatExam, carrier4, Arrays.asList("examDetailsId"));
        }
    }
}
