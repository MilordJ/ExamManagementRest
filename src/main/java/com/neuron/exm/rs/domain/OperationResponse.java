/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.domain;

import com.neuron.exm.rs.enums.ResultCode;


/**
 *
 * @author otahmadov
 */
public class OperationResponse {
    private ResultCode code;
    private String message;
    private Object data;

    public OperationResponse() {
    }

    public OperationResponse(ResultCode code) {
        this.code = code;
    }

    public OperationResponse(ResultCode code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ResultCode getCode() {
        return code;
    }

    public void setCode(ResultCode code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "OperationResponse{" + "code=" + code + ", message=" + message + ", data=" + data + '}';
    }
}
