/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.domain;

import com.neuron.exm.rs.annotation.DateFormat;
import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisTeacherEducationInfo extends CoreEntity {
    private String personId = "";
    private String orgId = "";
    private String eduLevelId = "";
    private String startActionId = "";
    @DateFormat
    private String startActionDate = "";
    private String endActionId = "";
    @DateFormat
    private String endActionDate = "";
}
