package com.neuron.exm.rs.domain;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisPersonLangSkill extends CoreEntity {
    private String personId = "";
    private String langId = "";
}
