/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.domain;

import com.neuron.exm.rs.annotation.DateFormat;
import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisPersonCompAccomplishments extends CoreEntity {
    private String personId = "";
    private String certificateName = "";
    private String certificateNumber = "";
    private String certificateGivenOrg = "";
    @DateFormat
    private String certificateGivenDate = "";
}
