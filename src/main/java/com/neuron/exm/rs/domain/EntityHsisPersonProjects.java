/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.domain;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisPersonProjects extends CoreEntity {
    private String personId = "";
    private String projectName = "";
    private String projectTypeId = "";
    private String projectStartYear = "";
    private String projectFinishYear = "";
    private String projectMission = "";
    private String projectDonor = "";
}
