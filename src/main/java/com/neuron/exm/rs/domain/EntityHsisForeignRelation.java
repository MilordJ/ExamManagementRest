/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.domain;

import com.neuron.exm.rs.annotation.DateFormat;
import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisForeignRelation extends CoreEntity {
    private String addrTreeId = "";
    private String companyId = "";
    @DateFormat
    private String startDate = "";
    @DateFormat
    private String endDate = "";
    private String note = "";
    private String orgId = "";
}
