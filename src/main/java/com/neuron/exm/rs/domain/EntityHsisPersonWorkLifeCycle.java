/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.domain;

import com.neuron.exm.rs.annotation.DateFormat;
import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisPersonWorkLifeCycle extends CoreEntity {
    private String personId = "";
    private String actTypeId = "";
    @DateFormat
    private String actDate = "";
    private String orgId = "";
    private String teaching = "";
    private String cardNum = "";
    private String staffTypeId = "";
    private String positionId = "";
    private String status = "";
    private String reasonId = "";
    private String note = "";
    private String endActionTypeId = "";
    private String endActionDate = "";
    private String contractTypeId = "";
}
