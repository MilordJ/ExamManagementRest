/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.domain;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisEduGroup extends CoreEntity {
    private String name = "";
    private String orgId = "";
    private String eduLevelId = "";
    private String groupType = "";
    private String eduTypeId = "";
}
