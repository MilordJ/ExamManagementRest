/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.domain;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisDiplomDetails extends CoreEntity {
    private String typeId = "";
    private String startNum = "";
    private String endNum = "";
    private String serial = "";
    private String eduYearId = "";
    private String eduLevelId = "";
    private String orgId = "";
    private String givenDate = "";
    private String note = "";
    private String orderId = "";
}
