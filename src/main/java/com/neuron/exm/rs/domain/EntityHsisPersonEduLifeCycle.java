/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.domain;

import com.neuron.exm.rs.annotation.DateFormat;
import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisPersonEduLifeCycle extends CoreEntity {
    private String personId = "";
    private String actTypeId = "";
    @DateFormat
    private String actDate = "";
    private String orgId = "";
    private String foreignOrgId = "";
    private String applyScore = "";
    private String eduLineId = "";
    private String eduTypeId = "";
    private String edupayTypeId = "";
    private String eduLangId = "";
    private String eduLevelId = "";
    private String cardNum = "";
    private String note = "";
    private String status = "";
    private String eduYearStartId = "";
    private String eduYearEndId = "";
    private String avgScore = "";
    private String specCode = "";
    private String reasonId = "";
    private String endActTypeId = "";
    @DateFormat
    private String endActDate = "";
    private String orderId = "";
    private String inOrderId = "";
    private String outOrderId = "";
    private String mdlOrderId = "";
    private String editable = "";
}
