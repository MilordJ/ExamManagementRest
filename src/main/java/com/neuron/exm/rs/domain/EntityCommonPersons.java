/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.domain;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityCommonPersons extends CoreEntity {
    private String pincode = "";
    private String birthdate = "";
    private String genderId = "";
    private String photoFileId = "";
    private String citizenshipId = "";
    private String socialStatusId = "";
    private String maritalStatusId = "";
    private String orphanDegreeId = "";
    private String militaryServiceId = "";
    private String nationalityId = "";
    private String disabilityDegreeId = "";
    private String fname = "";
    private String lname = "";
    private String mname = "";
    private String orgId = "";
    private String checkIamas = "";
}
