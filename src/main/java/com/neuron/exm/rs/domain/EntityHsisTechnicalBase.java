/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.domain;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisTechnicalBase extends CoreEntity {
    private String name = "";
    private String typeId = "";
    private String orgId = "";
    private String startDate = "";
    private String endDate = "";
    private String volume = "";
    private String area = "";
    private String parentId = "";
}
