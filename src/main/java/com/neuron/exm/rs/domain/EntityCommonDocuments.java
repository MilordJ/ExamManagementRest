/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.domain;

import com.neuron.exm.rs.annotation.DateFormat;
import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityCommonDocuments extends CoreEntity {
    private String docTypeId = "";
    private String docSerial = "";
    private String docNum = "";
    private String note = "";
    @DateFormat
    private String docStartDate = "";
    @DateFormat
    private String docEndDate = "";
    private String orgId = "";
    private String fileId = "";
}
