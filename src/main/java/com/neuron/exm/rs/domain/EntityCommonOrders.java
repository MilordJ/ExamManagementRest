/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.domain;

import com.neuron.exm.rs.annotation.DateFormat;
import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityCommonOrders extends CoreEntity {
    private String docTypeId = "";
    private String note = "";
    private String docNum = "";
    @DateFormat
    private String docStartDate= "";
    @DateFormat
    private String docEndDate= "";
    private String orgId= "";
    private String status= "";
    private String fileId= "";
    private String dicFormId= "";
    private String dicReasonId= "";
    private String eduLevelId= "";
}
