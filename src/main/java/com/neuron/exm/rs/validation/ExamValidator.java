/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.validation;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.EntityExamExamDetails;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityEmsV_CourseStudent_v4;
import com.neuron.cm.view.EntityExamV_ChangeableExams_v4;
import com.neuron.cm.view.EntityExamV_ExamDetails_v4;
import com.neuron.cm.view.EntityExamV_Tickets_v4;
import com.neuron.exm.rs.enums.RequiredConstants;

/**
 *
 * @author Lito
 */
public class ExamValidator {
    Class domainExamDetails = EntityExamExamDetails.class;
    
    public void getExamList() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
//        String uniId = SessionManager.getCurrentUserUni();
        String orgFormula = EntityManager.getOrgFormulaByParamOrCurrentUser();
        c.setValue("orgFormula", orgFormula);
        c.removeKey("orgId");
//        if (!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN")) {
////            c.setValue("orgId", uniId);
//        
//        }
        if (SessionManager.getCurrentUserType().equals("STUDENT")) {
            c.addError("userType", "Invalid user type");
        }
    }

    public void getExamParticipantList() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String examId = c.getValue("examDetailId").toString();
        if (SessionManager.getCurrentUserType().equals("STUDENT")) {
            c.addError("userType", "Invalid user type");
        }
        if (examId == null || examId.trim().isEmpty()) {
            c.addError("Invalid exam Id", examId);
            return;
        }
    }

    public void insertNewExam() throws Exception {
        ChangeFieldFormatValidator.changeDateFormat(domainExamDetails);
        Carrier c = SessionManager.sessionCarrier();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertExam());
        String uniId = SessionManager.getCurrentUserUni();
        if (!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN")) {
            c.setValue("orgId", uniId);
        }
        if (SessionManager.getCurrentUserType().equals("STUDENT")) {
            c.addError("userType", "Invalid user type");
        }
        
        String courseId = c.getValue("courseId").toString();
        String participants = "participants";
        String examTypeId = c.getValue("examTypeId").toString();
        
        Carrier carrier = new Carrier();
        carrier.setValue("courseId", courseId);
        carrier.setValue("examTypeId", examTypeId);
        EntityExamV_Tickets_v4 ent = new EntityExamV_Tickets_v4();
        int ticketCount = EntityManager.getRowCount(ent, carrier);
        if (ticketCount == 0) {
            c.addError("Invalid request", "insufficientTicketCount");
            return;
        }
        
        int partCount = c.getTableRowCount(participants);
        for (int i = 0; i < partCount; i++) {
            EntityEmsV_CourseStudent_v4 ent1 = new EntityEmsV_CourseStudent_v4();
            Carrier c1 = new Carrier();        
            c1.setValue("courseId", courseId);
            c1.setValue("id", c.getValue(participants, i, "participantId"));
            int rowCount = EntityManager.getRowCount(ent1, c1);
            if(rowCount == 0){
                c.addError("InvalidParticipant", "Participant has not found!");
                return;
            }        
        }
    }

    public void updateExam() throws Exception {
        ChangeFieldFormatValidator.changeDateFormat(domainExamDetails);
        Carrier c = SessionManager.sessionCarrier();
        String examId = c.getValue("id").toString();
        EntityExamV_ChangeableExams_v4 ent = new EntityExamV_ChangeableExams_v4();
        Carrier carrier = new Carrier();
        carrier.setValue("id", examId);
        int rowCount = EntityManager.getRowCount(ent, carrier);
        if (rowCount == 0) {
            c.addError("InvalidRequest", "Exam is used!");
            return;
        }
        
        String courseId = c.getValue("courseId").toString();
        String participants = "participants";
        int partCount = c.getTableRowCount(participants);
        for (int i = 0; i < partCount; i++) {
            EntityEmsV_CourseStudent_v4 ent1 = new EntityEmsV_CourseStudent_v4();
            Carrier c1 = new Carrier();        
            c1.setValue("courseId", courseId);
            c1.setValue("id", c.getValue(participants, i, "participantId"));
            int count = EntityManager.getRowCount(ent1, c1);            
            if(count == 0){
                c.addError("InvalidParticipant", "Participant has not found!");
                return;
            }        
        }
        
        c.removeKey("repetition");
    }

    public void deleteExam() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String examId = c.getValue("id").toString();
        EntityExamV_ChangeableExams_v4 ent = new EntityExamV_ChangeableExams_v4();
        Carrier carrier = new Carrier();
        carrier.setValue("id", examId);
        int rowCount = EntityManager.getRowCount(ent, carrier);
        if (rowCount == 0) {
            c.addError("InvalidRequest", "Exam is used!");
            return;
        }
        
        EntityExamV_ExamDetails_v4 examDetails_v4 = new EntityExamV_ExamDetails_v4();
        carrier = new Carrier();
        carrier.setValue("id", examId);
        String repetitionId = EntityManager.getColumnValueByColumnName(examDetails_v4, "repetition", carrier);
        c.setValue("repetitionId", repetitionId);
    }
}
