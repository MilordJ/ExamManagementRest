/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.validation;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.EntityCommonV_Dictionary_v4;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.enums.DictionaryCode;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityExamV_TicketQuestions_v4;
import com.neuron.exm.rs.enums.RequiredConstants;
/**
 *
 * @author Lito
 */
public class QuestionValidator {
//    String[] choisesVariant = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"};
    public void getQuestionList() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String uniId = SessionManager.getCurrentUserUni();
        if(!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN"))
            c.setValue("orgId", uniId);
    }
    public void insertNewQuestion() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertQuestion());
        String uniId = SessionManager.getCurrentUserUni();
        if(!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN"))
            c.setValue("orgId", uniId);
        EntityCommonV_Dictionary_v4 ent = new EntityCommonV_Dictionary_v4();
        Carrier carrier = new Carrier();
        carrier.setValue("id", c.getValue("questSpecId"));
        String code = EntityManager.getColumnValueByColumnName(ent, "code", carrier);
        if(!code.equals(DictionaryCode.QUESTION_CODE_OPEN)) {
            String tableName = "choises";
            int rowCount = c.getTableRowCount(tableName);
            if(rowCount == 0) {
                c.addErrorMessage("choisesCount", "Choises is empty");
                return;
            }
            int count = 0;
            for(int i = 0; i < rowCount; i++) {
                String rightChoises = c.getValue(tableName, i, "rightChoise").toString();
                String questChContent = c.getValue(tableName, i, "questChContent").toString();
                String questChFileId = c.getValue(tableName, i, "questChFileId").toString();
                if((questChContent == null ||questChContent.trim().isEmpty()) && (questChFileId == null ||questChFileId.trim().isEmpty())) {
                    c.addErrorMessage("invalidParams", "Content or fileId is empty");
                    return;
                }
                if(rightChoises.equals("1")) {
                    ++count; 
                }
            }
            if(count == 0) {
                c.addErrorMessage("rightChoise", "Right choise is not selected");
            }   
        } 
    }
    public void updateQuestion() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getUpdateQuestion());         
        String id = c.getValue("id").toString();
        Carrier carrier = new Carrier();
        carrier.setValue("id", id);
         if(id == null || id.trim().isEmpty()){
            c.addError("Invalid request", "Question is not found");
            return;
        }  
        EntityExamV_TicketQuestions_v4 ticketQuestions_v4 = new EntityExamV_TicketQuestions_v4();
        Carrier c1 = new Carrier();
        c1.setValue("questionId", id);
        int ticketQuestionRowCount = EntityManager.getRowCount(ticketQuestions_v4, c1);
        if(ticketQuestionRowCount>0){
            c.addError("Invalid request", "Question is used");
            return;
        }
        EntityCommonV_Dictionary_v4 ent = new EntityCommonV_Dictionary_v4();
        Carrier c2 = new Carrier();
        c2.setValue("id", c.getValue("questSpecId"));
        String code = EntityManager.getColumnValueByColumnName(ent, "code", c2);
        if(!code.equals(DictionaryCode.QUESTION_CODE_OPEN)) {
            String tableName = "choises";
            int rowCount = c.getTableRowCount(tableName);
            if(rowCount == 0) {
                c.addErrorMessage("choisesCount", "Choises is empty");
                return;
            }
            int count = 0;
            for(int i = 0; i < rowCount; i++) {
                String rightChoises = c.getValue(tableName, i, "rightChoise").toString();
                String questChContent = c.getValue(tableName, i, "questChContent").toString();
                String questChFileId = c.getValue(tableName, i, "questChFileId").toString();
                if((questChContent == null ||questChContent.trim().isEmpty()) && (questChFileId == null ||questChFileId.trim().isEmpty())) {
                    c.addErrorMessage("invalidParams", "Content or fileId is empty");
                    return;
                }
                if(rightChoises.equals("1")) {
                    ++count; 
                }
            }
            if(count == 0) {
                c.addErrorMessage("rightChoise", "Right choise is not selected");
            }   
        } 
        String uniId = SessionManager.getCurrentUserUni();
        if(!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN"))
            c.setValue("orgId", uniId);
    }
    public void deleteQuestion() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getDeleteQuestion());
        String id = c.getValue("id").toString();
        Carrier carrier = new Carrier();
        carrier.setValue("id", id);
        EntityExamV_TicketQuestions_v4 ticketQuestions_v4 = new EntityExamV_TicketQuestions_v4();
        Carrier c1 = new Carrier();
        c1.setValue("questionId", id);
        int ticketQuestionRowCount = EntityManager.getRowCount(ticketQuestions_v4, c1);
        if(ticketQuestionRowCount>0){
            c.addError("Invalid request", "Question is used");
            return;
        }
        String uniId = SessionManager.getCurrentUserUni();
        if(!SessionManager.getCurrentUserType().equals("SYSADMIN") && !SessionManager.getCurrentUserType().equals("ADMIN"))
            c.setValue("orgId", uniId);
    }
    
    public void getQuestionChoisesList() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String questId = c.getValue("questId").toString();
        if(SessionManager.getCurrentUserType().equals("STUDENT"))
            c.addError("userType", "Invalid user type");
        if(questId == null || questId.trim().isEmpty()){
            c.addError("Invalid quest Id", questId);
            return;
        }
    }

}
