/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.exm.rs.validation;

import com.neuron.cm.domain.Carrier;
import com.neuron.cm.domain.SessionManager;
import com.neuron.cm.util.EntityManager;
import com.neuron.cm.view.EntityExamV_ExamParticipant_v4;
import com.neuron.cm.view.EntityExamV_Questions_v4;
import com.neuron.exm.rs.enums.RequiredConstants;

/**
 *
 * @author Lito
 */
public class TicketsValidator {
    public void insertNewTicket() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        c.checkParams(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertTicket());
        String tableName = "structures";
        c.checkParamsTBL(((RequiredConstants) SessionManager.getRequiredConstants()).getInsertTicketQuestion(), tableName);
        int rowCount = c.getTableRowCount(tableName);
        if(rowCount == 0) {
            c.addError("structureCount", "Structure list is empty!!!");
            return;
        }
            
        for(int i = 0; i < rowCount; i++) {
            EntityExamV_Questions_v4 ent = new EntityExamV_Questions_v4();
            String questTopicId = c.getValue(tableName, i, "questTopicId").toString();
            String questSpecId = c.getValue(tableName, i, "questSpecId").toString();
            String questTypeId = c.getValue(tableName, i, "questTypeId").toString();
            String questLevelId = c.getValue(tableName, i, "questLevelId").toString();
            String eduLangId = c.getValue(tableName, i, "eduLangId").toString();
            int questCount = Integer.valueOf(c.getValue(tableName, i, "questCount").toString());
            
            Carrier carrier = new Carrier();
            carrier.setValue("questSpecId", questSpecId);
            carrier.setValue("questTypeId", questTypeId);
            carrier.setValue("questLevelId", questLevelId);
            carrier.setValue("eduLangId", eduLangId);
            ent.setAndStatementFildForIn("QUEST_TOPIC_ID", questTopicId);
            if(EntityManager.getRowCount(ent, carrier) < questCount) {
                c.addErrorMessage("QuestionCount", "Suallarin sayi Secilen parametrlere uygun gelmir!!!");
                return;
            }
            
        }
        
    }
 
    public void deleteTicket() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        EntityExamV_ExamParticipant_v4 ent = new EntityExamV_ExamParticipant_v4();
        String ticketId = c.getValue("id").toString();
        Carrier c1 = new Carrier();
        c1.setValue("ticketId", ticketId);
        int rowCount = EntityManager.getRowCount(ent, c1);
        if(rowCount>0){
            c.addError("invalidRequest", "Used ticket can't delete");
            return;
        }
    }
    
    public void getTicketsList() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        if(SessionManager.getCurrentUserType().equals("STUDENT"))
            c.addError("userType", "Invalid user type");
    }
    public void getTicketQuestions() throws Exception {
        Carrier c = SessionManager.sessionCarrier();
        String ticketId = c.getValue("ticketId").toString();
        if(SessionManager.getCurrentUserType().equals("STUDENT"))
            c.addError("userType", "Invalid user type");        
        if(ticketId == null || ticketId.trim().isEmpty()){
            c.addError("Invalid ticketId Id", ticketId);
            return;
        }
    }
}
