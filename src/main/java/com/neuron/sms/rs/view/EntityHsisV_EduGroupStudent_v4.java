/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_EduGroupStudent_v4 extends CoreEntity {
    private String groupId = "";
    private String groupName = "";
    private String groupEduLevelId = "";
    private String groupEduTypeId = "";
    private String groupOrgId = "";
    private String pelcId = "";
    private String personId = "";
    private String personFullname = "";
    private String searchKeyword = "";
}
