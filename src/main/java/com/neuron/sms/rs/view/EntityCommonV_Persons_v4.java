/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityCommonV_Persons_v4 extends CoreEntity {
    private String birthdate = "";
    private String citizenshipId = "";
    private String citizenshipNameAz = "";
    private String citizenshipNameEn = "";
    private String citizenshipNameRu = "";
    private String firstname = "";
    private String lastname = "";
    private String middlename = "";
    private String genderId = "";
    private String genderNameAz = "";
    private String genderNameEn = "";
    private String genderNameRu = "";
    private String maritalStatusId = "";
    private String maritalStatusNameAz = "";
    private String maritalStatusNameEn = "";
    private String maritalStatusNameRu = "";
    private String militaryServiceId = "";
    private String militaryServiceNameAz = "";
    private String militaryServiceNameEn = "";
    private String militaryServiceNameRu = "";
    private String nationalityId = "";
    private String nationalityNameAz = "";
    private String nationalityNameEn = "";
    private String nationalityNameRu = "";
    private String orphanDegreeId = "";
    private String orphanDegreeNameAz = "";
    private String orphanDegreeNameEn = "";
    private String orphanDegreeNameRu = "";
    private String photoFileId = "";
    private String pincode = "";
    private String socialStatusId = "";
    private String socialStatusNameAz = "";
    private String socialStatusNameEn = "";
    private String socialStatusNameRu = "";
    private String disabilityDegreeId = "";
    private String disabilityDegreeNameAz = "";
    private String disabilityDegreeNameEn = "";
    private String disabilityDegreeNameRu = "";
}
