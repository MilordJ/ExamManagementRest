/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_StudentForEduGroup_v4  extends CoreEntity {
    private String personId = "";
    private String citizenshipId = "";
    private String citizenshipParentId = "";
    private String fullname = "";
    private String searchKeyword = "";
    private String orgId = "";
    private String orgNameAz = "";
    private String orgNameEn = "";
    private String orgNameRu = "";
    private String orgFormula = "";
    private String eduTypeId = "";
    private String eduLevelId = "";
}
