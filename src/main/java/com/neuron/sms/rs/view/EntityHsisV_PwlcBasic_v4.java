/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_PwlcBasic_v4 extends CoreEntity {
    private String personId = "";
    private String fullname = "";
    private String actTypeId = "";
    private String actionTypeNameAz = "";
    private String actionTypeNameEn = "";
    private String actionTypeNameRu = "";
    private String actionDate = "";
    private String orgId = "";
    private String orgNameAz = "";
    private String orgNameEn = "";
    private String orgNameRu = "";
    private String orgFormula = "";
    private String teaching = "";
    private String staffTypeId = "";
    private String staffTypeNameAz = "";
    private String staffTypeNameEn = "";
    private String staffTypeNameRu = "";
    private String positionId = "";
    private String positionNameAz = "";
    private String positionNameEn = "";
    private String positionNameRu = "";
    private String status = "";
    private String statusNameAz = "";
    private String statusNameEn = "";
    private String statusNameRu = "";
    private String academicNameId = "";
    private String academicNameAz = "";
    private String academicNameEn = "";
    private String academicNameRu = "";
    private String academicDegreeId = "";
    private String academicDegreeAz = "";
    private String academicDegreeEn = "";
    private String academicDegreeRu = "";
}
