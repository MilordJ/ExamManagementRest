/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_BasicOrgTree_v4 extends CoreEntity {
    private String typeId = "";
    private String formula = "";
    private String parentId = "";
    private String nameAz = "";
    private String nameEn = "";
    private String nameRu = "";
    private String code = "";
    private String addressTreeId = "";
}
