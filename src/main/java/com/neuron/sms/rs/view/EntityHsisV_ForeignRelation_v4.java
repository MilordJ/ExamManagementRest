/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_ForeignRelation_v4 extends CoreEntity {
    private String addrTreeId = "";
    private String addressNameAz = "";
    private String addressNameEn = "";
    private String addressNameRu = "";
    private String addressFormula = "";
    private String companyId = "";
    private String companyNameAz = "";
    private String companyNameEn = "";
    private String companyNameRu = "";
    private String startDate = "";
    private String endDate = "";
    private String note = "";
    private String orgId = "";
}
