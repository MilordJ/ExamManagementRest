/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntitySecurityV_Users_v4 extends CoreEntity {
    private String personId = "";
    private String firstname = "";
    private String lastname = "";
    private String middlename = "";
    private String genderId = "";
    private String personBirthdate = "";
    private String genderNameAz = "";
    private String genderNameEn = "";
    private String genderNameRu = "";
    private String photoFileId = "";
    private String pincode = "";
    private String userName = "";
    private String currentLangId = "";
    private String langNameAz = "";
    private String langNameEn = "";
    private String langNameRu = "";
    private String userRoleId = "";
    private String roleNameAz = "";
    private String roleNameEn = "";
    private String roleNameRu = "";
    private String roleFormula = "";
    private String isBlocked = "";
    private String orgId = "";
    private String orgNameAz = "";
    private String orgNameEn = "";
    private String orgNameRu = "";
    private String orgFormula = "";
    private String orgTypeId = "";
    private String uniId = "";
    private String uniNameAz = "";
    private String uniNameEn = "";
    private String uniNameRu = "";
    private String uniLogoPath = "";
    private String uniCoverphotoFileId = "";
    private String userType = "";
    private String userAccountId = "";
    private String defaultAppId = "";
    private String defaultUserId = "";
            
}

