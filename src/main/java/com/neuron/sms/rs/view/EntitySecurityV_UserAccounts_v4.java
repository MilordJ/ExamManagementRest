/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntitySecurityV_UserAccounts_v4 extends CoreEntity {
    private String personId = "";
    private String firstname = "";
    private String lastname = "";
    private String middlename = "";
    private String genderId = "";
    private String genderNameAz = "";
    private String genderNameEn = "";
    private String genderNameRu = "";
    private String birthdate = "";
    private String pincode = "";
    private String photoFileId = "";
    private String userName = "";
    private String currentLangId = "";
    private String currentLangAz = "";
    private String currentLangEn = "";
    private String currentLangRu = "";
}
