/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_DiplomStudents_v4 extends CoreEntity {
    private String pelcId = "";
    private String orgId = "";
    private String orgNameAz = "";
    private String orgNameEn = "";
    private String orgNameRu = "";
    private String personId = "";
    private String fullname = "";
    private String searchKeyword = "";
    private String serial = "";
    private String num = "";
    private String diplomId = "";
    private String take = "";
}
