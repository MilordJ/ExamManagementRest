/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_TechnicalBaseDefinition_v4 extends CoreEntity {
    private String definitionId = "";
    private String definitionNameAz = "";
    private String definitionNameEn = "";
    private String definitionNameRu = "";
    private String technicalBaseId = "";
    
}
