/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_TeacherSubject_v4 extends CoreEntity {
    private String personId = "";
    private String subjDicId = "";
    private String subjectNameAz = "";
    private String subjectNameEn = "";
    private String subjectNameRu = "";
}
