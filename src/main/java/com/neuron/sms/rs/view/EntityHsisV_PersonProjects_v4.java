/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_PersonProjects_v4 extends CoreEntity {
    private String personId = "";
    private String projectName = "";
    private String projectTypeId = "";
    private String projectTypeNameAz = "";
    private String projectTypeNameEn = "";
    private String projectTypeNameRu = "";
    private String projectStartYear = "";
    private String projectFinishYear = "";
    private String projectMission = "";
    private String projectDonor = "";
}
