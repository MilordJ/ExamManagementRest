/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_StudentPassiveEducation_v4 extends CoreEntity {
    private String personId = "";
    private String pincode = "";
    private String actTypeId = "";
    private String actTypeNameAz = "";
    private String actTypeNameEn = "";
    private String actTypeNameRu = "";
    private String startActDate = "";
    private String orgId = "";
    private String orgNameAz = "";
    private String orgNameEn = "";
    private String orgNameRu = "";
    private String orgTypeNameAz = "";
    private String orgTypeNameEn = "";
    private String orgTypeNameRu = "";
    private String orgFormula = "";
    private String eduLineId = "";
    private String eduLineAz = "";
    private String eduLineEn = "";
    private String eduLineRu = "";
    private String eduTypeId = "";
    private String eduTypeAz = "";
    private String eduTypeEn = "";
    private String eduTypeRu = "";
    private String edupayTypeId = "";
    private String edupayTypeAz = "";
    private String edupayTypeEn = "";
    private String edupayTypeRu = "";
    private String eduLangId = "";
    private String eduLangAz = "";
    private String eduLangEn = "";
    private String eduLangRu = "";
    private String eduLevelId = "";
    private String eduLevelAz = "";
    private String eduLevelEn = "";
    private String eduLevelRu = "";
    private String cardNum = "";
    private String applyScore = "";
    private String endActDate = "";
    private String endActTypeId = "";
    private String endActTypeNameAz = "";
    private String endActTypeNameEn = "";
    private String endActTypeNameRu = "";
    
}
