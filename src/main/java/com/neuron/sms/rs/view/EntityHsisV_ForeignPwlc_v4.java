/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_ForeignPwlc_v4 extends CoreEntity {
    private String personId = "";
    private String fullname = "";
    private String citizenshipId = "";
    private String citizenshipNameAz = "";
    private String citizenshipNameEn = "";
    private String citizenshipNameRu = "";
    private String actTypeId = "";
    private String actionTypeNameAz = "";
    private String actionTypeNameEn = "";
    private String actionTypeNameRu = "";
    private String actionDate = "";
    private String orgId = "";
    private String orgNameAz = "";
    private String orgNameEn = "";
    private String orgNameRu = "";
    private String orgFormula = "";
    private String teaching = "";
    private String staffTypeId = "";
    private String staffTypeNameAz = "";
    private String staffTypeNameEn = "";
    private String staffTypeNameRu = "";
    private String positionId = "";
    private String positionNameAz = "";
    private String positionNameEn = "";
    private String positionNameRu = "";
    private String contact = "";
    private String address = "";
}
