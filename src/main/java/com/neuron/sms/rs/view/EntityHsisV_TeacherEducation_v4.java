/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_TeacherEducation_v4 extends CoreEntity {
    private String personId = "";
    private String orgId = "";
    private String orgNameAz = "";
    private String orgNameEn = "";
    private String orgNameRu = "";
    private String eduLevelId = "";
    private String eduLevelNameAz = "";
    private String eduLevelNameEn = "";
    private String eduLevelNameRu = "";
    private String startActionId = "";
    private String startActionNameAz = "";
    private String startActionNameEn = "";
    private String startActionNameRu = "";
    private String startActionDate = "";
    private String endActionId = "";
    private String endActionNameAz = "";
    private String endActionNameEn = "";
    private String endActionNameRu = "";
    private String endActionDate = "";
}
