/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author U
 */
@Data
public class EntityExamV_Questions_V4 extends CoreEntity {
    private String questContent = "";
    private String searchKeyword = "";
    private String questTopicId = "";
    private String topicNameAz = "";
    private String topicNameEn = "";
    private String topicNameRu = "";
    private String questLevelId = "";
    private String levelNameAz = "";
    private String levelNameEn = "";
    private String levelNameRu = "";
    private String questSpecId = "";
    private String specNameAz = "";
    private String specNameEn = "";
    private String specNameRu = "";
    private String eduPlanId = "";
    private String eduPlanName = "";
    private String eduLangId = "";
    private String eduLangNameAz = "";
    private String eduLangNameEn = "";
    private String eduLangNameRu = "";
    private String subjectId = "";
    private String subjectNameAz = "";
    private String subjectNameEn = "";
    private String subjectNameRu = "";
    private String questTypeId = "";
    private String questTypeNameAz = "";
    private String questTypeNameEn = "";
    private String questTypeNameRu = "";
    private String questFileId = "";
    private String fileOriginalName = "";
    private String fileNote = "";
    private String fileSize = "";
    private String filePath = "";
}
