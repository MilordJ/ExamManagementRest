/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_StudentEducationDetails_v4 extends CoreEntity {
    private String personId = "";
    private String orderId = "";
    private String orderTypeId = "";
    private String orderFormId = "";
    private String orderReasonId = "";
    private String orderNum = "";
    private String orderDate = "";
    private String eduLevelId = "";
    private String orderFileId = "";
    private String orderStatus = "";
    private String applyScore = "";
    private String cardNum = "";
    private String editable = "";
    private String eduLangId = "";
    private String eduLineId = "";
    private String eduTypeId = "";
    private String edupayTypeId = "";
    private String specId = "";
    private String specializationId = "";
}
