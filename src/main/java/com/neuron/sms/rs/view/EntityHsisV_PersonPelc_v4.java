/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_PersonPelc_v4 extends CoreEntity {
    private String pelcId = "";
    private String birthdate = "";
    private String checkIamas = "";
    private String citizenshipId = "";
    private String disabilityDegreeId = "";
    private String fname = "";
    private String lname = "";
    private String mname = "";
    private String genderId = "";
    private String maritalStatusId = "";
    private String militaryServiceId = "";
    private String nationalityId = "";
    private String orphanDegreeId = "";
    private String photoFileId = "";
    private String fileOriginalName = "";
    private String pincode = "";
    private String socialStatusId = "";
}
