/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityCommonV_Orders_v4 extends CoreEntity {
    private String docTypeId = "";
    private String docTypeAz = "";
    private String docTypeEn = "";
    private String docTypeRu = "";
    private String docTypeCode = "";
    private String note = "";
    private String docNum = "";
    private String docStartDate = "";
    private String docEndDate = "";
    private String eduYearId = "";
    private String eduYearName = "";
    private String orgId = "";
    private String orgNameAz = "";
    private String orgNameEn = "";
    private String orgNameRu = "";
    private String status = "";
    private String statusNameAz = "";
    private String statusNameEn = "";
    private String statusNameRu = "";
    private String eduLevelId = "";
    private String eduLevelNameAz = "";
    private String eduLevelNameEn = "";
    private String eduLevelNameRu = "";
    private String dicFormId = "";
    private String dicFormNameAz = "";
    private String dicFormNameEn = "";
    private String dicFormNameRu = "";
    private String dicFormCode = "";
    private String dicReasonId = "";
    private String dicReasonNameAz = "";
    private String dicReasonNameEn = "";
    private String dicReasonNameRu = "";
    private String dicReasonCode = "";
    private String fileId = "";
    private String fileContentType = "";
    private String filePath = "";
    private String fileOriginalName = "";
}
