/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_TechnicalBase_v4 extends CoreEntity {
    private String name = "";
    private String typeId = "";
    private String typeNameAz = "";
    private String typeNameEn = "";
    private String typeNameRu = "";
    private String orgId = "";
    private String orgNameAz = "";
    private String orgNameEn = "";
    private String orgNameRu = "";
    private String orgFormula = "";
    private String startDate = "";
    private String endDate = "";
    private String volume = "";
    private String area = "";
    private String parentId = "";
    private String parentName = "";
    
}
