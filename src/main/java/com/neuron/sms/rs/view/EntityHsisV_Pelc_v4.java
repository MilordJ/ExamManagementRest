/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_Pelc_v4 extends CoreEntity {
    private String personId = "";
    private String pincode = "";
    private String birthdate = "";
    private String genderId = "";
    private String genderNameAz = "";
    private String genderNameEn = "";
    private String genderNameRu = "";
    private String photoFileId = "";
    private String citizenshipId = "";
    private String citizenshipParentId = "";
    private String fullname = "";
    private String searchKeyword = "";
    private String inOrderId = "";
    private String actionTypeId = "";
    private String actionTypeAz = "";
    private String actionTypeEn = "";
    private String actionTypeRu = "";
    private String actionDate = "";
    private String orderNumber = "";
    private String uniId = "";
    private String uniNameAz = "";
    private String uniNameEn = "";
    private String uniNameRu = "";
    private String orgId = "";
    private String orgNameAz = "";
    private String orgNameEn = "";
    private String orgNameRu = "";
    private String orgFormula = "";
    private String applyScore = "";
    private String eduLineId = "";
    private String eduLineAz = "";
    private String eduLineEn = "";
    private String eduLineRu = "";
    private String eduTypeId = "";
    private String eduTypeAz = "";
    private String eduTypeEn = "";
    private String eduTypeRu = "";
    private String edupayTypeId = "";
    private String edupayTypeAz = "";
    private String edupayTypeEn = "";
    private String edupayTypeRu = "";
    private String eduLangId = "";
    private String eduLangAz = "";
    private String eduLangEn = "";
    private String eduLangRu = "";
    private String eduLevelId = "";
    private String eduLevelAz = "";
    private String eduLevelEn = "";
    private String eduLevelRu = "";
    private String status = "";
    private String statusNameAz = "";
    private String statusNameEn = "";
    private String statusNameRu = "";
    private String cardNum = "";
    private String eduYearStartId = "";
    private String eduYearName = "";
    private String mdlOrderId = "";
    private String outOrderId = "";
    private String editable = "";
    private String orgDicId = "";
}
