/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.annotation.DateFormat;
import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_PassivePelcDocuments_v4 extends CoreEntity {
    private String passivePelcId = "";
    private String docId = "";
    private String docTypeId = "";
    private String docTypeAz = "";
    private String docTypeEn = "";
    private String docTypeRu = "";
    private String docDicTypeId = "";
    private String docSerial = "";
    private String docNum = "";
    @DateFormat
    private String docStartDate = "";
    @DateFormat
    private String docEndDate = "";
    private String docFileId = "";
    private String docFileOriginalName = "";
}
