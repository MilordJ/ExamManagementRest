/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuron.sms.rs.view;

import com.neuron.exm.rs.util.CoreEntity;
import lombok.Data;

/**
 *
 * @author otahmadov
 */
@Data
public class EntityHsisV_DplmEduStudent_v4 extends CoreEntity {
    private String uniCode= "";
    private String uniName= "";
    private String fullname= "";
    private String name = "";
    private String surname = "";
    private String patronymic = "";
    private String passSeria = "";
    private String passNum = "";
    private String passPin = "";
    private String birthdate = "";
    private String birthCity = "";
    private String iamasAddress = "";
    private String address = "";
    private String genderName = "";
    private String phone = "";
    private String startEduYear = "";
    private String startSpecCode = "";
    private String startSpecName = "";
    private String endSpecCode = "";
    private String endSpecName = "";
    private String eduDegreeName = "";
    private String eduLangName = "";
    private String eduFormName = "";
    private String paymentTypeName = "";
    private String commandTqdkName = "";
    private String commandName = "";
    private String commandTypeName = "";
    private String diplomTypeName = "";
    private String diplomSeria = "";
    private String diplomNo = "";
    private String studentEduTypeName = "";
    private String diplomGivenTypeName = "";
    private String commandLastName = "";
    private String groupName = "";
    private String applyScore = "";
    private String schoolCode = "";
    private String schoolName = "";
}
